import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleReader {

    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private String inputString;
    ProjectManager projectManager = new ProjectManager();
    TaskManager taskManager = new TaskManager();

    public void readCommand() {
        do {
            try {
                inputString = reader.readLine();
                switch (inputString) {
                    case "project-create": projectManager.create();
                        break;
                    case "project-list": projectManager.list();
                        break;
                    case "project-update": projectManager.update();
                        break;
                    case "project-remove": projectManager.remove();
                        break;
                    case "project-clear": projectManager.clear();
                        break;

                    case "task-create": taskManager.create();
                        break;
                    case "task-list": taskManager.list();
                        break;
                    case "task-update": taskManager.update();
                        break;
                    case "task-remove": taskManager.remove();
                        break;
                    case "task-clear": taskManager.clear();
                        break;

                    case "exit": break;
                    case "help": System.out.println("help: Show all commands.");
                        projectManager.help();
                        taskManager.help();
                        System.out.print("\n");
                        break;
                    default: System.out.println("Command not found.");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (!"exit".equals(inputString));
    }
}
