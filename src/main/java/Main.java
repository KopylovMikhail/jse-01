/**
 * @author Mikhail Kopylov
 * task/project manager
 */

public class Main {
    public static void main(String[] args) {
        ConsoleReader consoleReader = new ConsoleReader();
        consoleReader.readCommand();
    }
}
