public interface Manageable {

    void create();

    void list();

    void update();

    void remove();

    void clear();

    void help();

}
