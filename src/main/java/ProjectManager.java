import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.TreeSet;

public class ProjectManager implements Manageable {
    Set<String> projects = new TreeSet<>();
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void create() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        try {
            projects.add(reader.readLine());
            System.out.println("[OK]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void list() {
        System.out.println("[PROJECT LIST]");
        int i = 1;
        for (String name : projects) {
            System.out.println(i++ + ". " + name);
        }
        System.out.print("\n");
    }

    public void update() {
        System.out.println("[PROJECT UPDATE]");
        System.out.println("ENTER EXISTING PROJECT NAME:");
        try {
            String oldName = reader.readLine();
            if (projects.contains(oldName)) {
                projects.remove(oldName);
                System.out.println("ENTER NEW PROJECT NAME:");
                String newName = reader.readLine();
                projects.add(newName);
                System.out.println("[PROJECT " + oldName + " UPDATED TO " + newName + "]\n");
            } else System.out.println("SUCH A PROJECT DOES NOT EXIST.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void remove() {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER EXISTING PROJECT NAME:");
        try {
            String name = reader.readLine();
            if (projects.contains(name)) {
                projects.remove(name);
                System.out.println("[PROJECT " + name + " REMOVED]\n");
            } else System.out.println("SUCH A PROJECT DOES NOT EXIST.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clear() {
//        System.out.println("[PROJECT CLEAR]");
        projects.clear();
        System.out.println("[ALL PROJECTS REMOVED]\n");
    }

    @Override
    public void help() {
        System.out.println("project-create: Create new project.\n" +
                "project-list: Show all projects.\n" +
                "project-update: Update selected project.\n" +
                "project-remove: Remove selected project.\n" +
                "project-clear: Remove all projects.");
    }
}
