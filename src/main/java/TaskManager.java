import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.TreeSet;

public class TaskManager implements Manageable {
    Set<String> tasks = new TreeSet<>();
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        try {
            tasks.add(reader.readLine());
            System.out.println("[OK]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void list() {
        System.out.println("[TASK LIST]");
        int i = 1;
        for (String name : tasks) {
            System.out.println(i++ + ". " + name);
        }
        System.out.print("\n");
    }

    public void update() {
        System.out.println("[TASK UPDATE]");
        System.out.println("ENTER EXISTING TASK NAME:");
        try {
            String oldName = reader.readLine();
            if (tasks.contains(oldName)) {
                tasks.remove(oldName);
                System.out.println("ENTER NEW TASK NAME:");
                String newName = reader.readLine();
                tasks.add(newName);
                System.out.println("[TASK " + oldName + " UPDATED TO " + newName + "]\n");
            } else System.out.println("SUCH A TASK DOES NOT EXIST.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void remove() {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER EXISTING TASK NAME:");
        try {
            String name = reader.readLine();
            if (tasks.contains(name)) {
                tasks.remove(name);
                System.out.println("[TASK " + name + " REMOVED]\n");
            } else System.out.println("SUCH A TASK DOES NOT EXIST.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clear() {
//        System.out.println("[TASK CLEAR]");
        tasks.clear();
        System.out.println("[ALL TASKS REMOVED]\n");
    }

    @Override
    public void help() {
        System.out.println("task-create: Create new task.\n" +
                "task-list: Show all tasks.\n" +
                "task-update: Update selected task.\n" +
                "task-remove: Remove selected task.\n" +
                "task-clear: Remove all tasks.");
    }
}
